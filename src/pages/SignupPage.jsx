import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import Signup from '../components/Signup/Signup';

const SignupPage = () => {
    const navigate = useNavigate();
    
    return (
      <div>
          <Signup />
      </div>
    )
  }

export default SignupPage